﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace IntegerIncrementRegexer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer;
        public MainWindow()
        {
            InitializeComponent();
            Init(); 
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(300);
            timer.Tick += timer_Tick;
        }

        private void SubValue(int subValue)
        {
            if (ParseValue(subValue))
            { 
                int.TryParse(IncValues.Content.ToString(), out int newInt);
                IncValues.Content = newInt - 1;
            }
        }

        private void AddValue(int addValue)
        {
            if (ParseValue(addValue))
            {
                int.TryParse(IncValues.Content.ToString(), out int newInt);
                IncValues.Content = newInt + 1;
            }

        }

        private bool ParseValue(int val)
        {
            string src = Src.Text;
            string searchedNum = SrcRegex.Text;

            timer.Stop();
            if (src.Length == 0)
            {
                Src.Background = Brushes.Salmon;
            }

            var success = double.TryParse(searchedNum, out double original);

            if (!success || searchedNum.Length == 0)
            {
                SrcRegex.Background = Brushes.Salmon;
            }

            var isNewValue = double.TryParse(NextValue.Text, out double nextValue);
            nextValue = !isNewValue ? original + val : nextValue + val;

            int cnt = 0;
            string dest = Regex.Replace(src, searchedNum, m => {
                cnt++;
                return nextValue.ToString();
            });

            if (cnt == 0)
            {
                SrcRegex.Background = Brushes.Salmon;
            } 


            if (!success || cnt == 0 || src.Length == 0)
            {
                timer.Start();
                return false;
            }

            FoundRegex.Content = cnt.ToString();
            NextValue.Text = nextValue.ToString();
            SrcRegex.Background = Brushes.WhiteSmoke;
            NextValue.Background = Brushes.White;
            Dest.Text = dest;
            Clipboard.SetDataObject(dest);
            DateInc.Content = DateTime.Now.ToString("HH:mm:ss");

            return true;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            NextValue.Background = Brushes.WhiteSmoke;
            SrcRegex.Background = Brushes.White;
            Src.Background = Brushes.White;
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            Init();
        }

        private void Init()
        {
            NextValue.Background = Brushes.WhiteSmoke;
            SrcRegex.Background = Brushes.White;
            Src.Background = Brushes.White;
            NextValue.IsReadOnly = true;
            SrcRegex.IsReadOnly = false;
            NextValue.Text = "";
            Dest.Text = "";
            DateInc.Content = "";
            IncValues.Content = 0;
        }

        private void AddOne_Click(object sender, RoutedEventArgs e)
        {
            AddValue(1);
        }

        private void AddFive_Click(object sender, RoutedEventArgs e)
        {
            AddValue(5);
        }

        private void AddTen_Click(object sender, RoutedEventArgs e)
        {
            AddValue(10);
        }

        private void OnTop_Checked(object sender, RoutedEventArgs e)
        {
            this.Topmost = true;
        }

        private void RemoveOne_Click(object sender, RoutedEventArgs e)
        {
            SubValue(-1);
        }

        private void RemoveFive_Click(object sender, RoutedEventArgs e)
        {
            SubValue(-5);
        }

        private void RemoveTen_Click(object sender, RoutedEventArgs e)
        {
            SubValue(-10);
        }
    }
}
